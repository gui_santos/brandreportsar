﻿using Dlp.Buy4.Repository.Base;
using System.Collections.Generic;

namespace ReportsAR
{
    public interface IMembersRepository : IRepository<Members>
    {
        List<Members> GetMembersList();
    }
}