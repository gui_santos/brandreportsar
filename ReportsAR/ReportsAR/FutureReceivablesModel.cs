﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportsAR
{
    [Table("FutureReceivables2", Schema = "dbo")]
    public class FutureReceivablesModel
    {
        [Key]
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public DateTime ProcessDate { get; set; }
        public DateTime SettlementDate { get; set; }
        public int Issuer { get; set; }
        public int ConsecutiveDays { get; set; }
        public int Brand { get; set; }
        public int Company { get; set; }
        public decimal Irf { get; set; }
        public string reportName { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedAt { get; set; }
    }
}
