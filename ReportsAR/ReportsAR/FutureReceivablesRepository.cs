﻿using Dlp.Buy4.Repository.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportsAR
{
    public sealed class FutureReceivablesRepository:Repository<FutureReceivablesModel>, IFutureReceivablesRepository
    {
        Database _database;

        public FutureReceivablesRepository(Buy4_boContext dbContext) : base(dbContext)
        {
            this._database = dbContext.Database;
        }


        public FutureReceivablesRepository(DbContext context) : base(context) { }

        public List<FutureReceivablesModel> GetReportList(string reportName)
        {
            string sqlFutureReceivables = $@"SELECT
                                                    A.[Id]
                                                  ,A.[Amount]
                                                  ,A.[ProcessDate]
                                                  ,A.[SettlementDate]
                                                  ,A.[Issuer]
                                                  ,A.[ConsecutiveDays]
                                                  ,A.[Brand]
                                                  ,A.[Company]
                                                  ,A.[Irf]
                                                  ,A.[reportName]
                                                  ,A.[CreatedAt]
                                              FROM [BD_FINANCEIRO].[dbo].[FutureReceivables] A(nolock)
                                              Where A.[reportName] = '{reportName}'";

            try
            {
                List<FutureReceivablesModel> openOperationList = this._database.SqlQuery<FutureReceivablesModel>(sqlFutureReceivables).ToList();
                return openOperationList;
            }
            catch (Exception e)
            {
                Console.WriteLine("++++ ERROR SQL : " + e.Message);
            }
            return new List<FutureReceivablesModel>();
        }
    }
}
