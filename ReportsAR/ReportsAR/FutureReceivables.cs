﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportsAR
{
    public class FutureReceivable
    {
        public double amount { get; set; }
        public DateTime ProcessDate { get; set; }
        public DateTime settlementDate { get; set; }
        public Member issuerName { get; set; }
        public int consecutiveDays { get; set; }
        public int brand { get; set; }
        public Member company { get; set; }
        //public long transactionCount { get; set; }
        public double irf { get; set; }
        //public int workdays { get; set; }
        public string reportName { get; set; }

        public FutureReceivable(DateTime date,double amount, DateTime settlementDate, Member issuer, int brand, Member company, double irf, string reportName)
        {
            this.amount = amount;
            this.ProcessDate = date;
            this.settlementDate = settlementDate;
            this.issuerName = issuer;
            this.consecutiveDays = (int)(settlementDate - date).TotalDays;
            this.brand = brand;
            this.company = company;
            this.irf = irf;
            this.reportName = reportName;
            //this.transactionCount = transactionCount;
        }
    }
}
