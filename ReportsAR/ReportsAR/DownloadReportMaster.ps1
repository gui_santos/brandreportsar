﻿Add-PSSnapin ShareFile

################################################################################
# DownloadFiles.ps1
#
# This script will download all files from the My Files and Folders area of 
# ShareFile to the local Documents directory.
#

#Run the following interactively to create a login token that can be used by Get-SfClient in unattended scripts
#$sfClient = New-SfClient -Name ((Join-Path $env:USERPROFILE "Documents") + "\YourSubdomain.sfps") -Account YourSubdomain
$sfClient = Get-SfClient -Name (".\YourSubdomain.sfps")

#ShareFile directory is relative to the root of the account
#get the current user's home folder to use as the starting point
$ShareFileHomeFolder = "sfdrive:/Rogerio.Hong@mastercard.com/Stone/Stone201703/relatorio_settlement_" + (Get-Date -format yyyyMMdd) + ".xlsx"
#$ShareFileHomeFolder = "sfdrive:/Rogerio.Hong@mastercard.com/Stone/Stone201703/relatorio_settlement_20170614.xlsx"


#use the local My Documents folder
$LocalPath = '\\dfs2\Shared\Financial\Tesouraria\Relatórios de recebimento das bandeiras\'+ (Get-Date -format yyyyMMdd)

#New-Item -ItemType directory -Path $LocalPath


#Create a PowerShell provider for ShareFile at the location specified
New-PSDrive -Name sfDrive -PSProvider ShareFile -Client $sfClient -Root "/"

#download files from a sub-folder in ShareFile to a local folder
#path must be specified (can't use root) so make sure to map the provider at a level higher than you want to copy from
Copy-SfItem -Path $ShareFileHomeFolder -Destination $LocalPath -Force 1

#Remove the PSProvider when we are done
Remove-PSDrive sfdrive
