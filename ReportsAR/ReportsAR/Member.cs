﻿namespace ReportsAR
{
    public class Member
    {
        public int Id { get; set; }
        public string BDName { get; set; }
        public string BrandReportName { get; set; }
        public int OrigBrand { get; set; }
    }
}