﻿using Dlp.Buy4.Repository.Base;
using ReportsAR;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportsAR
{
    public sealed class MembersRepository:Repository<Members>, IMembersRepository
    {
        Database _database;

        public MembersRepository(Buy4_boContext dbContext) : base(dbContext)
        {
            this._database = dbContext.Database;
        }

        public List<Members> GetMembersList()
        {
            string sqlMembers = @"SELECT [MemberId]
                                          ,[LegalName]
                                          ,[TradingName]
                                          ,[Cnpj]
                                          ,[PostalCode]
                                          ,[Address]
                                          ,[City]
                                          ,[State]
                                          ,[Uf]
                                          ,[CountryCode]
                                          ,[IsBuyer]
                                          ,[IsIssuer]
                                          ,[Neighborhood]
                                          ,[BankingCode]	
                                          ,[NeedsPaymentNotification]
                                      FROM [asgn].[Member] (nolock)";
            try
            {
                List<Members> membersList = this._database.SqlQuery<Members>(sqlMembers).ToList();
                return membersList;
            }
            catch (Exception e)
            {
                Console.WriteLine("++++ ERROR SQL : " + e.Message);
            }
            return new List<Members>();
        }
    }
}
