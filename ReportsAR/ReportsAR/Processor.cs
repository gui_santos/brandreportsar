﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using Outlook = Microsoft.Office.Interop.Outlook;


namespace ReportsAR
{
    class Processor
    {

        private readonly IUnitOfWork _unitOfWork;

        public bool success { get; set; }
        public Processor()
        {
            _unitOfWork = new UnitOfWork(); ;
        }

        public List<FutureReceivable> readVisaReport(string filePath, List<FutureReceivable> futureReceivables, List<Member> banks)
        {
            Member company;

            Excel.Application xlapp = new Excel.Application();
            xlapp.Visible = true;
            xlapp.DisplayAlerts = false;
            Excel.Workbooks workbook = xlapp.Workbooks;
            Excel.Workbook report = workbook.Open(filePath);

            Excel._Worksheet deffered = (Excel.Worksheet)report.Worksheets["DEFERRED SETTLEMENT"];
            Excel._Worksheet installment = (Excel.Worksheet)report.Worksheets["INSTALLMENT SETTLEMENT"];

            Excel.Range lastdef = deffered.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
            Excel.Range rangedef = deffered.get_Range("A1", lastdef);
            int lastUsedRowDef = lastdef.Row;
            int lastUsedColumnDef = lastdef.Column;

            Excel.Range lastinst = installment.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
            Excel.Range rangeinst = installment.get_Range("A1", lastinst);
            int lastUsedRowInst = lastinst.Row;
            int lastUsedColumnInst = lastinst.Column;

            rangedef.UnMerge();
            rangeinst.UnMerge();

            if (filePath.Contains("19588"))
            {
                company = banks.Where(x => x.BDName.Contains("Elavon")).First();
                readVisaDeffered(lastUsedRowDef, lastUsedColumnDef, deffered, futureReceivables, company, banks, Path.GetFileName(filePath));
                readVisaInstallment(lastUsedRowInst, lastUsedColumnInst, installment, futureReceivables, company, banks, Path.GetFileName(filePath));
            }
            else if (filePath.Contains("44301"))
            {
                company = banks.Where(x => x.BDName.Contains("Stone")).First();
                readVisaDeffered(lastUsedRowDef, lastUsedColumnDef, deffered, futureReceivables, company, banks, Path.GetFileName(filePath));
                readVisaInstallment(lastUsedRowInst, lastUsedColumnInst, installment, futureReceivables, company, banks, Path.GetFileName(filePath));
            }

            return futureReceivables;
        }

        public void readVisaDeffered(int lastRow, int lastColumn, Excel._Worksheet sheet, List<FutureReceivable> futureReceivables, Member company, List<Member> banks, string reportName)
        {
            Member issuer;
            DateTime reportDate;
            double amount;
            double irf;
            DateTime date;
            Console.WriteLine("deffered");
            reportDate = DateTime.ParseExact(sheet.Cells[5, 11].Value, "d-MMM-yy", CultureInfo.InvariantCulture);
            date = reportDate;

            //Excel.Range range = sheet.get_Range[sheet.Cells[1][1], sheet.Cells[lastRow][lastColumn]];
            Excel.Range range = sheet.Range[sheet.Cells[1, 1], sheet.Cells[lastRow, 11]];
            object[,] values = (object[,])range.Value2;

            for (int row = 1; row <= lastRow; row++)
            {

                if (values[row, 1]?.ToString() == "DAILY PENDING BY PRODUCT")
                {
                    if ((values[row, 4])?.ToString() == "ALL ISSUERS")
                        break;

                    issuer = banks.Where(x => x.OrigBrand == 1 && x.BrandReportName == ((values[row, 4].ToString()).Substring(23))).First();


                    for (row += 1; row <= lastRow; row++)
                    {
                        if (values[row, 1]?.ToString() == "TOTAL PENDING BY PRODUCT")
                            break;

                        if (values[row, 3]?.ToString() != null)
                        {
                            try
                            {
                                date = DateTime.ParseExact(values[row, 3].ToString(), "dd-MMM-yy",
                                      CultureInfo.InvariantCulture);
                            }
                            catch
                            {
                                date = DateTime.ParseExact(values[row, 3].ToString(), "d-MMM-yy",
                                      CultureInfo.InvariantCulture);
                            }
                        }

                        if (values[row, 1]?.ToString() == "Total Day")
                        {
                            //transactionCount = (long)values[row, 6].Value;
                            amount = (double)values[row, 7];
                            irf = (double)values[row, 8];
                            FutureReceivable receivable = new FutureReceivable(reportDate, amount, date, issuer, (int)BrandEnum.Visa, company, irf, reportName);
                            futureReceivables.Add(receivable);
                            Console.WriteLine(company + " deffered % complete: " + ((row * 100) / lastRow) + "\r\n");
                        }
                    }
                }
            }
        }

        public void readVisaInstallment(int lastRow, int lastColumn, Excel._Worksheet sheet, List<FutureReceivable> futureReceivables, Member company, List<Member> banks, string reportName)
        {
            Member issuer;
            DateTime reportDate;
            double amount;
            int transactionCount;
            double irf;
            DateTime date;
            reportDate = DateTime.ParseExact(sheet.Cells[5, 9].Value, "d-MMM-yy", CultureInfo.InvariantCulture);
            date = reportDate;

            Excel.Range range = sheet.Range[sheet.Cells[1, 1], sheet.Cells[lastRow, 9]];
            object[,] values = (object[,])range.Value2;

            for (int row = 1; row <= lastRow; row++)
            {
                if (values[row, 1]?.ToString() == "DAILY PENDING")
                {
                    if ((values[row, 3]?.ToString()) == " ALL ISSUERS")
                        break;

                    issuer = banks.Where(x => x.OrigBrand == 1 && x.BrandReportName == (values[row, 3])?.ToString().Substring(23)).First();

                    for (row += 1; row <= lastRow; row++)
                    {
                        if (values[row, 1]?.ToString() == "TOTAL PENDING BY PRODUCT")
                            break;

                        if (values[row, 1]?.ToString() != null)
                        {
                            if ((values[row, 1]?.ToString()).Substring(0, 5) == "TOTAL")
                                break;
                            date = DateTime.ParseExact(values[row, 1]?.ToString(), "M/d/yyyy", CultureInfo.InvariantCulture);
                            transactionCount = (int)((double)values[row, 4]);
                            amount = (double)values[row, 5];
                            irf = (double)values[row, 6];
                            FutureReceivable receivable = new FutureReceivable(reportDate, amount, date, issuer, (int)BrandEnum.Visa, company, irf, reportName);
                            futureReceivables.Add(receivable);
                            Console.WriteLine(company + " installment % complete: " + ((row * 100) / lastRow) + "\r\n");
                        }
                    }
                }
            }
        }

        public void readMasterReport(string filePath, List<FutureReceivable> futureReceivables, List<Member> banks, string reportName)
        {

            Member issuer;
            DateTime reportDate;
            double amount;
            //int transactionCount;
            double icFee;
            DateTime date;
            Member company;

            //Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            //reportDate = DateTime.ParseExact(sheet.Cells[5, 9].Value, "dd-MMM-yy", CultureInfo.InvariantCulture);
            //date = reportDate;

            Excel.Application xlapp = new Excel.Application();
            xlapp.Visible = true;
            xlapp.DisplayAlerts = false;
            Excel.Workbooks workbook = xlapp.Workbooks;
            Excel.Workbook report = workbook.Open(filePath);
            Excel._Worksheet tabela = (Excel.Worksheet)report.Worksheets["Domestico"];
            Excel._Worksheet pivot = (Excel.Worksheet)report.Worksheets["pivot"];

            reportDate = pivot.Cells[2, 4].Value;

            //Excel.Range last = tabela.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
            Excel.Range last = tabela.Range["A" + tabela.Rows.Count.ToString()];
            //Excel.Range last = tabela.UsedRange;
            //Excel.Range range = tabela.get_Range("A1", last);
            //int lastUsedRow = last.Row;
            //int lastUsedColumn = last.Column;
            int lastUsedRow = last.End[Excel.XlDirection.xlUp].Row;

            Excel.Range range = tabela.get_Range($"A2:I{lastUsedRow}");
            object[,] values = (object[,])range.Value2;


            for (int row = 2; row <= values.GetLength(0); row++)
            {
                if (values[row, 1] == null)
                    break;
                try
                {
                    issuer = banks.Where(x => x.OrigBrand == 2 && x.BrandReportName == values[row, 1].ToString()).First();
                    amount = (double)values[row, 7];
                    //transactionCount = (int)values[row, 9];
                    icFee = (double)values[row, 8];
                    date = DateTime.FromOADate((double)values[row, 3]);
                    switch ((int)((double)values[row, 4]))
                    {
                        case 13485:
                            company = banks.Where(x => x.BDName.Contains("Elavon")).First(); ;
                            break;
                        case 14788:
                            company = banks.Where(x => x.BDName.Contains("Stone")).First(); ;
                            break;
                        default:
                            company = null;
                            break;
                    }

                    Console.WriteLine("Master % complete: " + ((row * 100) / lastUsedRow) + "\r\n");
                    FutureReceivable receivable = new FutureReceivable(reportDate, amount, date, issuer, (int)BrandEnum.Master, company, icFee, reportName);
                    futureReceivables.Add(receivable);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"{e.Message}");
                }
            }
        }

        public List<Member> LoadBanksFromExcel()
        {
            List<Member> banks = new List<Member>();

            Excel.Application xlapp = new Excel.Application();
            xlapp.Visible = false;
            xlapp.DisplayAlerts = false;
            Excel.Workbooks workbook = xlapp.Workbooks;
            Excel.Workbook report = workbook.Open(@"C:\Users\gcsantos\Desktop\Relação Grupos Bancarios DB.xlsx");

            Excel._Worksheet visa = (Excel.Worksheet)report.Worksheets["Visa"];
            Excel._Worksheet master = (Excel.Worksheet)report.Worksheets["Master"];

            Excel.Range lastVisa = visa.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
            Excel.Range rangeVisa = visa.get_Range("A1", lastVisa);
            int lastUsedRowVisa = lastVisa.Row;
            int lastUsedColumnVisa = lastVisa.Column;

            Excel.Range lastMaster = master.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
            Excel.Range rangeMaster = master.get_Range("A1", lastMaster);
            int lastUsedRowMaster = lastMaster.Row;
            int lastUsedColumnMaster = lastMaster.Column;

            for (int i = 2; i <= lastUsedRowVisa; i++)
            {
                banks.Add(new Member()
                {
                    Id = (int)visa.Cells[i, 1].Value,
                    BDName = visa.Cells[i, 2].Value,
                    BrandReportName = visa.Cells[i, 3].Value ?? "#N/A",
                    OrigBrand = 1,
                });
            }

            for (int i = 2; i <= lastUsedRowMaster; i++)
            {
                banks.Add(new Member()
                {
                    Id = (int)master.Cells[i, 1].Value,
                    BDName = master.Cells[i, 2].Value,
                    BrandReportName = master.Cells[i, 3].Value ?? "#N/A",
                    OrigBrand = 2,
                });
            }

            workbook.Close();
            xlapp.Quit();

            return banks;
        }


        public void CommitToBD(List<FutureReceivable> futureReceivables)
        {
            string report = futureReceivables.First().reportName;
            Console.WriteLine("Salvando");
            try
            {
                foreach (FutureReceivable receivable in futureReceivables)
                {
                    this._unitOfWork.FutureReceivablesRepository.Add(new FutureReceivablesModel()
                    {
                        Amount = (decimal)receivable.amount,
                        Brand = receivable.brand,
                        Company = receivable.company.Id,
                        ConsecutiveDays = receivable.consecutiveDays,
                        Irf = (decimal)receivable.irf,
                        ProcessDate = receivable.ProcessDate,
                        Issuer = receivable.issuerName.Id,
                        SettlementDate = receivable.settlementDate,
                        reportName = receivable.reportName
                    });
                }
                this._unitOfWork.Commit();
                this.success = true;
            }
            catch (Exception e)
            {
                SendCommitErrorMail(e, report);
                this._unitOfWork.Dispose();
            }
        }

        public void SendCommitErrorMail(Exception e, string report)
        {

            Outlook.Application app = new Outlook.Application();
            Outlook.MailItem mailItem = app.CreateItem(Outlook.OlItemType.olMailItem);
            mailItem.Subject = "[ERROR]Brands Exposure Reports Error " + (DateTime.Now.ToString());
            mailItem.To = "projetosfinanceiros@stone.com.br";

            mailItem.HTMLBody = $@"<br> Ocorreu um erro ao tenter presistir os dados do relátorio {report} de recebimentos das bandeiras!!!<br><br>
                                O sistema retornou a seguinte exception: {e.Message}<>br<br>";

            mailItem.Importance = Outlook.OlImportance.olImportanceHigh;

            mailItem.Send();
        }

        public void SendSuccessMail(string filename)
        {

            Outlook.Application app = new Outlook.Application();
            Outlook.MailItem mailItem = app.CreateItem(Outlook.OlItemType.olMailItem);
            mailItem.Subject = "Brands Exposure Reports Commited" + (DateTime.Now.ToString());
            mailItem.To = "projetosfinanceiros@stone.com.br";

            mailItem.HTMLBody = $@"<br> O relatório {filename} foi persistido com sucesso!!!<br><br>";

            mailItem.Importance = Outlook.OlImportance.olImportanceHigh;

            mailItem.Send();
        }

        public void SendNotReadedReportMail(string filename, bool alreadyExisted)
        {

            Outlook.Application app = new Outlook.Application();
            Outlook.MailItem mailItem = app.CreateItem(Outlook.OlItemType.olMailItem);
            mailItem.Subject = "Brands Exposure Reports Commited" + (DateTime.Now.ToString());
            mailItem.To = "projetosfinanceiros@stone.com.br";

            if (alreadyExisted)
                mailItem.HTMLBody = $@"<br> O relatório {filename} não foi persistido em {DateTime.Now.ToString()} pois já constava no banco de dados!!!<br><br>";
            else
                mailItem.HTMLBody = $@"<br> O relatório {filename} não foi persistido em {DateTime.Now.ToString()}!!!<br><br>";

            mailItem.Importance = Outlook.OlImportance.olImportanceHigh;

            mailItem.Send();
        }

        public void ReadReports()
        {
            List<FutureReceivable> futureReceivables;
            List<Member> banks = LoadBanksFromExcel();
            string filepath = $@"\\172.16.104.20\Shared\Financial\Tesouraria\Relatórios de recebimento das bandeiras\{DateTime.Now.ToString("yyyMMdd")}";
            DirectoryInfo d = new DirectoryInfo(filepath);


            Console.WriteLine("Init time" + DateTime.Now);
            foreach (var file in d.GetFiles().Where(x => !x.Name.Contains("~")))
            {
                success = false;
                bool alreadyExisted = false;

                if (_unitOfWork.FutureReceivablesRepository.GetReportList(file.Name).Count() == 0)
                {
                    if (file.Name.Contains("Credit Risk Exposure"))
                    {
                        futureReceivables = new List<FutureReceivable>();
                        readVisaReport(Path.Combine(filepath, file.Name), futureReceivables, banks);
                        CommitToBD(futureReceivables);
                    }

                    else if (file.Name.Contains("relatorio_settlement"))
                    {
                        futureReceivables = new List<FutureReceivable>();
                        readMasterReport(Path.Combine(filepath, file.Name), futureReceivables, banks, file.Name);
                        CommitToBD(futureReceivables);
                    }
                }
                else
                    alreadyExisted = true;

                if (success == false)
                {
                    //SendNotReadedReportMail(file.Name, alreadyExisted);
                    Console.WriteLine("Fail");
                }

                else if (success == true)
                {
                    SendSuccessMail(file.Name);
                    Console.WriteLine("Sucess");
                }
            }
            Console.WriteLine("End time" + DateTime.Now);
        } 
    }
}