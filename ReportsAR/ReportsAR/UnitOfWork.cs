﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportsAR
{
    public class UnitOfWork: IUnitOfWork
    {
        bool _disposed = false;

        private readonly Buy4_boContext _backOfficeContext;

        public UnitOfWork(Buy4_boContext backofficeContext = null)
        {
            if (_backOfficeContext == null)
            {
                _backOfficeContext = new Buy4_boContext();
            }
            else
            {
                _backOfficeContext = backofficeContext;
            }
        }

        //private IMembersRepository _membersRepository;
        //public IMembersRepository MembersRepository
        //{
        //    get { return _membersRepository ?? (this._membersRepository = new MembersRepository(this._backOfficeContext)); }
        //}

        private IFutureReceivablesRepository _futureReceivablesRepository;
        public IFutureReceivablesRepository FutureReceivablesRepository
        {
            get { return _futureReceivablesRepository ?? (this._futureReceivablesRepository = new FutureReceivablesRepository(this._backOfficeContext)); }
        }

        public void Commit()
        {
            _backOfficeContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                if (_disposed) return;

                _backOfficeContext.Dispose();

                _disposed = true;
            }

            _disposed = true;
        }

    }
}
