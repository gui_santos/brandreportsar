﻿using Dlp.Buy4.Repository.Base;
using System.Collections.Generic;

namespace ReportsAR
{
    public interface IFutureReceivablesRepository: IRepository<FutureReceivablesModel>
    {
        List<FutureReceivablesModel> GetReportList(string reportName);
    }
}