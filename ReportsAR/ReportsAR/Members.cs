﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportsAR
{
    public class Members
    {
        public int MemberId { get; set; }
        public string LegalName { get; set; }
        public string TradingName { get; set; }
        public string Cnpj { get; set; }
        public string PostalCode { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string CountryCode { get; set; }
        public Boolean IsBuyer { get; set; }
        public Boolean IsIssuer { get; set; }
        public string Neighborhood { get; set; }
        public int BankingCode { get; set; }
        public Boolean NeedsPaymentNotification { get; set; }
    }
}
