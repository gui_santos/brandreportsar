﻿namespace ReportsAR
{
    public interface IUnitOfWork
    {
        //IMembersRepository MembersRepository { get; }
        IFutureReceivablesRepository FutureReceivablesRepository { get; }
        void Dispose();
        void Commit();
    }
}