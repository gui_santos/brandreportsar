﻿using System;
using System.Management.Automation;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace ReportsAR
{
    public class ProcessorMaster
    {

        public void GetMasterReport()
        {
            using (PowerShell PS = PowerShell.Create())
            {
                var localPath = $@"\\dfs2\Shared\Financial\Tesouraria\Relatórios de recebimento das bandeiras\{DateTime.Now.ToString("yyyMMdd")}";
                Directory.CreateDirectory(localPath);

                var sfps = $@"{Application.StartupPath}\YourSubdomain.sfps";
                var fileName = $@"relatorio_settlement_{DateTime.Now.ToString("yyyMMdd")}.xlsx";

                ExecuteFromPowerShell($@"-File ""{Application.StartupPath}\DownloadReportMaster.ps1""");


                if (!File.Exists($@"{localPath}\{fileName}"))
                {
                    Console.WriteLine("Arquivo MASTER não encontrado : " + fileName);
                }
            }
        }



        /// <summary>
        /// Executa um comando no powershell, aguardando 600s para terminar.
        /// </summary>
        /// <param name="cmd">string do comando</param>
        /// <returns></returns>
        public void ExecuteFromPowerShell(string cmd)
        {
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                CreateNoWindow = false,
                FileName = "powershell",
                Arguments = cmd

            };

            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit(10 * 60 * 1000);

            process.Close();
        }



    }
}
