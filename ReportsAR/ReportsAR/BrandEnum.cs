﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportsAR
{
        public enum BrandEnum
        {
            /// <summary>
            /// Bandeira Visa.
            /// </summary>
            Visa = 1,

            /// <summary>
            /// Bandeira Master Card.
            /// </summary>
            Master = 2
    }
}
